package com.training.watertracker;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {


    private static onRecyclerItemClickListener recyclerItemClickListener;

    public MyAdapter(ArrayList<Boisson> boissonArrayList, onRecyclerItemClickListener _listener){
        Singleton.getInstance().boissons=boissonArrayList;
        recyclerItemClickListener = _listener;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_glass_type, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Boisson boissonAtPosition = Singleton.getInstance().boissons.get(position);
        holder.textView.setText(boissonAtPosition.getNomBoisson());
        holder.imageView.setImageResource(boissonAtPosition.getDrawable_photo());
    }

    @Override
    public int getItemCount() {
        return Singleton.getInstance().boissons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView imageView;
        public TextView textView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView_fragment_icon);
            textView = itemView.findViewById(R.id.text_view_fragment_drink_name);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            recyclerItemClickListener.onClickListener(view, getAdapterPosition());
        }
    }
}
