package com.training.watertracker;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Fragment_Live extends Fragment {

    private final Handler mainHandler = new Handler();
    TextView textView_currentTaux;
    TextView textView_countdownTimer;
    TextView textView_sanctions;
    CardView cardView_uber;

    CountDownTimer mCountdownTimer;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment__live, container, false);

        textView_countdownTimer = rootView.findViewById(R.id.timer_to_drive);
        textView_sanctions = rootView.findViewById(R.id.text_view_sanctions);
        cardView_uber = rootView.findViewById(R.id.cardView_uber);

        textView_currentTaux = rootView.findViewById(R.id.currentTaux);
        DecimalFormat df = new DecimalFormat("#.##");
        String strDouble = df.format(Singleton_Taux_alcool.getInstance().getTaux_alcool());
        textView_currentTaux.setText("Taux Actuel : "+strDouble);
        startThread(rootView);
        calculateTime();
        setSanctions();


        //lorsque l'on clique sur reserver un uber, lancement de l'application Uber
        //si on a pas uber, renvoit vers playstore pour telecharger Uber
        cardView_uber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String url = "uber://?action=setPickup";
                    Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
                    startActivity(intent);
                }catch (android.content.ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.ubercab")));

                }
            }
        });


        return rootView;

    }

    private void setSanctions() {
        if (Singleton_Taux_alcool.getInstance().getTaux_alcool()>0.5
                && Singleton_Taux_alcool.getInstance().getTaux_alcool()<0.8){
            textView_sanctions.setText("-Entre 150 et 750€ d'amende\n-Véhicule Immobilisé\n-6 points en moins");
        }
        else if (Singleton_Taux_alcool.getInstance().getTaux_alcool()>0.8){
            textView_sanctions.setText("-Amende jusqu'à 4500€\n-Jusqu'à 2ans de prison\n-Possible suspension de permis (3ans ou plus)");
        }

    }


    public void startThread(View view){
        Fragment_Live.Runnable runnable = new Fragment_Live.Runnable();
        new Thread(runnable).start();
    }


    class Runnable implements java.lang.Runnable {


        @Override
        public void run() {
            while (true) {
                Log.d("Debug thread", Singleton_Taux_alcool.getInstance().getRapport_taux() + "");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mainHandler.post(new java.lang.Runnable() {
                    @Override
                    public void run() {

                        DecimalFormat df = new DecimalFormat("#.##");
                        String strDouble = df.format(Singleton_Taux_alcool.getInstance().getTaux_alcool());
                        textView_currentTaux.setText("Taux Actuel : "+strDouble);
                    }
                });
            }
        }

    }


    //Methode pour calculer le temps restant où la personne sera au dessus du seuil
    private void calculateTime() {

        double tauxDiff = Singleton_Taux_alcool.getInstance().taux_alcool-0.5;
        if (tauxDiff>0){
            final long[] timeInMillis = {(long) (tauxDiff * 900000 / 0.025)};
            mCountdownTimer = new CountDownTimer(timeInMillis[0], 1000) {
                @Override
                public void onTick(long l) {

                    timeInMillis[0] = l;
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("H:mm:ss");

                    textView_countdownTimer.setText(sdf.format(timeInMillis[0]));
                }

                @Override
                public void onFinish() {
                }
            }.start();
        }

    }
}