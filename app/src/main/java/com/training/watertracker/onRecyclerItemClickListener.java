package com.training.watertracker;

import android.view.View;

public interface onRecyclerItemClickListener {

    void onClickListener(View view, int position);
}
