package com.training.watertracker;

public class Singleton_Person {

    public Person current_person;


    public Singleton_Person(Person person){
        this.current_person = person;
    }
    public Singleton_Person() {}

    public static final Singleton_Person instance = new Singleton_Person();
    public static final Singleton_Person getInstance(){return instance;}


    public Person getCurrent_person() {
        return current_person;
    }

    public void setCurrent_person(Person current_person) {
        this.current_person = current_person;
    }
}
