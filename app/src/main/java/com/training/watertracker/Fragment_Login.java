package com.training.watertracker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


public class Fragment_Login extends Fragment {

    private FirebaseAuth mAuth;
    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance("https://profilepage-afbad-default-rtdb.europe-west1.firebasedatabase.app/");


    Button button_login, button_register;
    EditText editText_pseudo, editText_password;
    ProgressBar progressBar;
    CallBackFragment callBackFragment;
    String user_pseudo,user_password;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView =  inflater.inflate(R.layout.fragment__login, container, false);
        editText_pseudo = rootView.findViewById(R.id.et_pseudo);
        editText_password = rootView.findViewById(R.id.et_password);
        button_login = rootView.findViewById(R.id.btn_login);
        button_register = rootView.findViewById(R.id.btn_register);
        progressBar = rootView.findViewById(R.id.progressBar_Fragment_login);
        mAuth = FirebaseAuth.getInstance();


        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginUser();
            }
        });


        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(callBackFragment!=null){
                    callBackFragment.change();
                }
            }
        });
        return rootView;
    }




    private Boolean validateUserName(){
        user_pseudo = editText_pseudo.getText().toString().trim();
        if (user_pseudo.isEmpty()){
            editText_pseudo.setError("Vous avez oublié de renseigner votre mail !");
            return false;
        }else {
            editText_pseudo.setError(null);
            progressBar.setVisibility(View.GONE);
            return true;
        }
    }

    private Boolean validateUserPassword(){

        user_password = editText_password.getText().toString().trim();

        if(user_password.isEmpty()){
            editText_password.setError("Vous avez oublié de renseigner votre mot de passe !");
            return false;
        }else{
            editText_password.setError(null);
            progressBar.setVisibility(View.GONE);
            return true;
        }

    }


    private void loginUser() {

        if (!validateUserName() | !validateUserPassword()) {
            return;
        } else {
            progressBar.setVisibility(View.VISIBLE);
            isUser();
            progressBar.setVisibility(View.GONE);
        }


    }

    private void isUser() {
        String userEnteredUsername = editText_pseudo.getText().toString().trim();
        String userEnteredPassword = editText_password.getText().toString().trim();


        DatabaseReference reference = firebaseDatabase.getReference("Users");

        Query checkUser = reference.orderByChild("nom_complet").equalTo(userEnteredUsername);

        checkUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {

                    editText_pseudo.setError(null);
                    String passwordFromDB = snapshot.child(userEnteredUsername).child("password").getValue(String.class);

                    if (passwordFromDB.equals(userEnteredPassword)) {

                        String emailFromDB = snapshot.child(userEnteredUsername).child("email").getValue(String.class);
                        int phoneFromDB = snapshot.child(userEnteredUsername).child("phone_number").getValue(Integer.class);
                        int poidsFromDB = snapshot.child(userEnteredUsername).child("poids").getValue(Integer.class);
                        int sexeFromDB = snapshot.child(userEnteredUsername).child("sexe").getValue(Integer.class);

                        Person loginPerson = new Person(userEnteredUsername, emailFromDB, poidsFromDB, phoneFromDB, sexeFromDB);
                        Singleton_Person.getInstance().setCurrent_person(loginPerson);
                        setUserName(getContext(), userEnteredUsername);
                        Intent intent = new Intent(getContext(), MainActivity.class);

                        startActivity(intent);
                    } else {
                        editText_password.setError("Mot de passe incorrect");
                        editText_password.requestFocus();
                    }
                } else {
                    editText_pseudo.setError("Aucun utilisateur avec ce pseudo");
                    editText_pseudo.requestFocus();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });




    }


    public void setCallBackFragment(CallBackFragment callBackFragment){
           this.callBackFragment = callBackFragment;
    }

    static final String PREF_USER_NAME= "";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserName(Context ctx, String userName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static String getUserName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }
}