package com.training.watertracker;

import java.io.Serializable;

public class Person implements Serializable {

    private String nom_complet;
    private String password;
    private String email;
    private int poids;
    private int phone_number;
    private int sexe;
    private double taux_alcool;
    @Override
    public String toString() {
        return "Person{" +
                "nom_complet='" + nom_complet + '\'' +
                ", email='" + email + '\'' +
                ", poids=" + poids +
                ", phone_number=" + phone_number +
                ", sexe=" + sexe +
                '}';
    }

    public Person(String nom_complet, String email, int poids,
                  int phone_number, int sexe) {
        this.nom_complet = nom_complet;
        this.email = email;
        this.poids = poids;
        this.phone_number = phone_number;
        this.sexe = sexe;
    }

    public Person(String nom_complet, int sexe , String password,
                  String email, int poids, int phone_number) {
        this.nom_complet = nom_complet;
        this.password = password;
        this.sexe = sexe;
        this.email = email;
        this.poids = poids;
        this.phone_number = phone_number;
        this.taux_alcool = Singleton_Taux_alcool.instance.getTaux_alcool();
    }


    public String getNom_complet() {
        return nom_complet;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public int getPoids() {
        return poids;
    }

    public int getPhone_number() {
        return phone_number;
    }

    public int getSexe() {
        return sexe;
    }
}
