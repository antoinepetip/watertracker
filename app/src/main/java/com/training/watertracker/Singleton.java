package com.training.watertracker;

import android.util.Log;

import java.util.ArrayList;

public class Singleton {

    public ArrayList<Boisson> boissons;
    Boisson beer = new Boisson("Pinte de binouze",5, R.drawable.beer_icon, 50);
    Boisson wine = new Boisson("Verre de vin", 13, R.drawable.glass_wine, 15);
    Boisson champagne = new Boisson("Verre de champagne",12, R.drawable.champagne, 18);
    Boisson cocktail = new Boisson("Un martini", 15, R.drawable.martini, 17);
    Boisson whisky = new Boisson("Un sky", 40, R.drawable.whisky, 5);
    Boisson vodka = new Boisson("Vodka", 40, R.drawable.vodka, 5);


    private static final Singleton instance = new Singleton();

    private Singleton()
    {
        System.out.println("Construction du Singleton au premier appel");
        boissons = new ArrayList<>();
        boissons.add(champagne);
        boissons.add(beer);
        boissons.add(wine);
        boissons.add(cocktail);
        boissons.add(vodka);

    }

    public static final Singleton getInstance()
    {
        return instance;
    }



    public static void log4Me(String message, String functionName){
        Log.d("Debug Antoine", functionName + " " + message);
    }
}
