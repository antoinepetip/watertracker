package com.training.watertracker;

import static com.training.watertracker.Fragment_Login.setUserName;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;
import java.util.concurrent.Executor;

public class Fragment_register_2 extends Fragment {

    String TAG = "Debug Antoine Register";
    EditText tvPassword;
    Button btnRegister;
    ImageView img1,img2,img3,img4;
    String user_password;
    Person transition_person;
    Person current_perso;

    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance("https://profilepage-afbad-default-rtdb.europe-west1.firebasedatabase.app/");
    DatabaseReference databaseReference = firebaseDatabase.getReference("Users");



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_register_2, container, false);
        tvPassword = rootView.findViewById(R.id.et_register_password);

        btnRegister = rootView.findViewById(R.id.btnRegister);

        img1 = rootView.findViewById(R.id.img1);
        img2 = rootView.findViewById(R.id.img2);
        img3 = rootView.findViewById(R.id.img3);
        img4 = rootView.findViewById(R.id.img4);

        tvPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (hasLength(s)){
                    img1.setImageResource(R.drawable.checked);
                }else{
                    img1.setImageResource(R.drawable.unchecked);
                }

                if (hasSymbol(s)){
                    img4.setImageResource(R.drawable.checked);
                }else{
                    img4.setImageResource(R.drawable.unchecked);
                }

                if (hasDigit(s)){
                    img3.setImageResource(R.drawable.checked);
                }else{
                    img3.setImageResource(R.drawable.unchecked);
                }

                if (hasUppercase(s)){
                    img2.setImageResource(R.drawable.checked);
                }else{
                    img2.setImageResource(R.drawable.unchecked);
                }

                if (hasLength(s) && hasUppercase(s) && hasDigit(s) && hasSymbol(s)){
                    btnRegister.setVisibility(View.VISIBLE);
                }else{
                    btnRegister.setVisibility(View.GONE);
                }



            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });

        return rootView;
    }

    private void registerUser() {
        Log.d(TAG, transition_person.toString());
        user_password = tvPassword.getText().toString();

        //If we have succeeded adding the user we set the singleton Person
        current_perso = new Person(transition_person.getNom_complet(), transition_person.getSexe(),
                user_password, transition_person.getEmail(), transition_person.getPoids(), transition_person.getPhone_number());
        Singleton_Person.getInstance().setCurrent_person(current_perso);


        databaseReference.child(current_perso.getNom_complet()).setValue(current_perso);


        Toast.makeText(getContext(), "Vous etes enregistré !", Toast.LENGTH_LONG).show();
        setUserName(getContext(), current_perso.getNom_complet());
        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
    }



    public boolean hasLength(CharSequence value){
        return String.valueOf(value).length() >=8;
    }

    public boolean hasDigit(CharSequence value){
        return String.valueOf(value).matches("(.*\\d.*)");
    }

    public boolean hasUppercase(CharSequence value){
        String s = String.valueOf(value);
        return !s.equals(s.toLowerCase());
    }

    public boolean hasSymbol(CharSequence value){
        String s = String.valueOf(value);
        return !s.matches("[A-Za-z0-9 ]*");
    }


    public void updatePerson(Person person){
        //here I have the transition person
        transition_person=person;
    }



}
