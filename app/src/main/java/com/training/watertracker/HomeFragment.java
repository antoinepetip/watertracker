package com.training.watertracker;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.training.watertracker.services.Alcohol_updater;

import java.text.DecimalFormat;


public class HomeFragment extends Fragment implements onRecyclerItemClickListener{


    private String TAG  = "Debug Antoine";
    RecyclerView recyclerView;
    ProgressBar progressBar;
    SharedPreferences sp;
    Person currentPerson;
    AlertDialog alertDialog;
    private final Handler mainHandler = new Handler();
    @Override
    public void onCreate(Bundle savedInstanceState) {super.onCreate(savedInstanceState);}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        //we get the current person in the Singleton
        currentPerson = Singleton_Person.getInstance().getCurrent_person();





        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        progressBar = rootView.findViewById(R.id.progressBar);
        recyclerView = rootView.findViewById(R.id.recycler_view_alcool_drinks);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        recyclerView.setAdapter(new MyAdapter(Singleton.getInstance().boissons,this));
        startThread(rootView);

        //set the progress bar value
        int rapport_en_pourcentage = Singleton_Taux_alcool.getInstance().getRapport_tauxFromValue(Singleton_Taux_alcool.instance.getTaux_alcool());
        Log.d(TAG, Double.toString(rapport_en_pourcentage));
        progressBar.setProgress(
                rapport_en_pourcentage
                ,true);




        return rootView;
    }


    public void startThread(View view){
        Runnable runnable = new Runnable();
        new Thread(runnable).start();
    }

    @Override
    public void onClickListener(View view, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        Boisson boisson_clique = Singleton.getInstance().boissons.get(position);


        /**
        AlertDialog builder for confirmation of drink
         */

        // Set Title and Message:
        builder.setTitle("Confirmation").setMessage("Avez-vous bu "+ boisson_clique.getNomBoisson());
        builder.setCancelable(true);
        builder.setIcon(boisson_clique.getDrawable_photo());

        // Create "Positive" button with OnClickListener.
        builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                // Si l'utilisateur clique sur oui


                //Start Service for alcohol
                getActivity().startService(new Intent(getActivity(),Alcohol_updater.class));

                double volume_mL = boisson_clique.getcL()*10;
                double pourcentage_alcool = boisson_clique.getPourcentage_alcool()/100.0;
                if (currentPerson.getSexe()==1){
                    Singleton_Taux_alcool.getInstance().setTaux_alcool
                            (Singleton_Taux_alcool.instance.getTaux_alcool() +
                                    ((volume_mL*pourcentage_alcool*0.8)/(0.7*currentPerson.getPoids())));
                    Log.d("Debug Antoine current person", currentPerson.toString());
                    Log.d("Debug Antoine taux", Double.toString(Singleton_Taux_alcool.instance.getTaux_alcool()));
                }
                else if (currentPerson.getSexe()==0){
                    Singleton_Taux_alcool.getInstance().setTaux_alcool((volume_mL*pourcentage_alcool*0.8)/(0.6*currentPerson.getPoids()));
                    Log.d("Debug Antoine current person", currentPerson.getNom_complet());
                    Log.d("Debug Antoine taux", Double.toString(Singleton_Taux_alcool.getInstance().getTaux_alcool()));
                }
                DecimalFormat df = new DecimalFormat("#.##");
                String strDouble = df.format(Singleton_Taux_alcool.getInstance().getTaux_alcool());
                Toast.makeText(getContext(), "Votre taux actuel est de : "+strDouble+ " g/L", Toast.LENGTH_SHORT).show();
                Log.d("Debug Antoine UpdateProgress", Singleton_Taux_alcool.getInstance().getRapport_taux()+"");
                int rapport_en_pourcentage = Singleton_Taux_alcool.getInstance().getRapport_tauxFromValue(Singleton_Taux_alcool.instance.getTaux_alcool());
                Log.d(TAG, Double.toString(rapport_en_pourcentage));
                progressBar.setProgress(
                        rapport_en_pourcentage
                        ,true);


            }

        });


        // Create "Negative" button with OnClickListener.
        builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //  Cancel
                dialog.cancel();
            }
        });

        // Create AlertDialog:
        AlertDialog alert = builder.create();
        alert.show();






    }


    class Runnable implements java.lang.Runnable{



        //Update the progress bar every 5minutes
        @Override
        public void run() {
            while(true){

                try {
                    Log.d("Debug Antoine UpdateProgress", Singleton_Taux_alcool.getInstance().getRapport_taux()+"");
                    Thread.sleep(300000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mainHandler.post(new java.lang.Runnable() {
                    @Override
                    public void run() {
                        progressBar.setProgress(Singleton_Taux_alcool.getInstance().getRapport_taux());
                    }
                });
            }
        }

    }





}