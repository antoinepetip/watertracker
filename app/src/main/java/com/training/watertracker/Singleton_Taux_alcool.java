package com.training.watertracker;

public class Singleton_Taux_alcool {

    public double taux_alcool;
    public int rapport_taux;


    public Singleton_Taux_alcool(double taux_alcool){
        this.taux_alcool = taux_alcool;
    }

    public static final Singleton_Taux_alcool instance = new Singleton_Taux_alcool();
    public static final Singleton_Taux_alcool getInstance()
    {
        return instance;
    }

    public void setTaux_alcool(double taux_alcool) {
        this.taux_alcool = taux_alcool;
    }

    public double getTaux_alcool() {
        return taux_alcool;
    }

    public int getRapport_tauxFromValue(double taux) {
        double taux_max = 0.80;
        double rapport_en_pourcentage = taux*100/taux_max;
        rapport_taux=(int) Math.round(rapport_en_pourcentage);
        return(int) Math.round(rapport_en_pourcentage);
    }

    public int getRapport_taux() {
        double taux_max = 0.80;
        double rapport_en_pourcentage = Singleton_Taux_alcool.getInstance().taux_alcool*100/taux_max;
        rapport_taux=(int) Math.round(rapport_en_pourcentage);
        return rapport_taux;
    }

    public void setRapport_taux(int rapport_taux) {
        this.rapport_taux = rapport_taux;
    }

    public Singleton_Taux_alcool() {
    }



}
