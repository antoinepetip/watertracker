package com.training.watertracker;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class Fragment_Register extends Fragment implements SendPerson {

    private EditText tvMail, complete_name, et_poids, et_phone;
    private Button btnNext;
    private RadioGroup radioGroup_register_sexe;
    private RadioButton radioButton_register_homme;
    private RadioButton radioButton_register_femme;
    private SendPerson mCallBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =inflater.inflate(R.layout.fragment__register, container, false);

        tvMail = rootView.findViewById(R.id.et_register_mail);
        complete_name = rootView.findViewById(R.id.et_register_Complete_Name);
        et_poids = rootView.findViewById(R.id.et_register_poids);
        et_phone = rootView.findViewById(R.id.et_register_phone);
        radioGroup_register_sexe = rootView.findViewById(R.id.radio_button_sexe);
        radioButton_register_homme = rootView.findViewById(R.id.radio_button_sexe_homme);
        radioButton_register_femme = rootView.findViewById(R.id.radio_button_sexe_femme);
        btnNext = rootView.findViewById(R.id.btnNext_for_register);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int checkedRadioId = radioGroup_register_sexe.getCheckedRadioButtonId();
                if(checkedRadioId==-1){
                    radioButton_register_homme.setError("Choisissez un sexe");
                    radioButton_register_femme.setError("");
                }
                else if (complete_name.getText().toString().length()==0){
                    complete_name.setError("Attention vous n'avez pas renseigné votre nom complet !");
                }
                else if (tvMail.getText().toString().length()==0 || !isValidEmail(tvMail.getText().toString())){
                    tvMail.setError("Attention vous n'avez pas renseigné un mail correct !");
                }
                else if (et_phone.getText().toString().length()==0 || et_phone.getText().toString().contains("+") ){
                    et_phone.setError("Attention vous n'avez pas renseigné votre numero de téléphone correctement!");
                }
                else if (et_poids.getText().toString().length()==0){
                    et_poids.setError("Attention vous n'avez pas renseigné votre poids !");
                }
                else{
                    String name = complete_name.getText().toString();
                    String mail = tvMail.getText().toString();
                    int sexe;

                    switch (radioGroup_register_sexe.getCheckedRadioButtonId()) {
                        case R.id.radio_button_sexe_homme:
                            sexe = 1;
                            break;
                        case R.id.radio_button_sexe_femme:
                            sexe= 0;
                            break;
                        default:
                            throw new IllegalStateException("Unexpected value: " + radioGroup_register_sexe.getCheckedRadioButtonId());
                    }
                    int poids = Integer.parseInt(et_poids.getText().toString());
                    int phone = Integer.parseInt(et_phone.getText().toString());

                    Person transition_person = new Person(name, mail, poids, phone, sexe);
                    mCallBack.sendPerson(transition_person);
                }

            }
        });


        return rootView;
    }

    private boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        // Pour etre sur que l'acitivité qui est le container a implementé
        // la callback interface. Sinon ca renvoit une exception
        try {
            mCallBack = (SendPerson) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallBack = null;
        super.onDetach();

    }

    @Override
    public void sendPerson(Person person) {
    }
}