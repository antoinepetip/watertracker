package com.training.watertracker;

import static android.app.Activity.RESULT_OK;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.cast.framework.media.ImagePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.training.watertracker.services.Alcohol_updater;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Objects;


public class Profile_Fragment extends Fragment {

    private static final String TAG = "Debug Antoine profile fragment";
    Person currentPerson;
    ImageView profile_photo, logout;
    FloatingActionButton fab_for_picture;
    LinearLayout linearLayout_facebook;
    TextView email;
    TextView phone_number;
    TextView poids;
    TextView name;
    TextView sexe;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    @Override
    public void onAttach(@NonNull Context context) {
        sharedPreferences = context.getSharedPreferences("userFile",Context.MODE_PRIVATE);
        editor =sharedPreferences.edit();

        super.onAttach(context);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_profile_, container, false);
        profile_photo = rootview.findViewById(R.id.profile_picture);
        email = rootview.findViewById(R.id.email_person);
        phone_number = rootview.findViewById(R.id.textView_person_phone);
        poids = rootview.findViewById(R.id.textView_person_poids);
        fab_for_picture = rootview.findViewById(R.id.floating_change_picture);
        logout = rootview.findViewById(R.id.logout);
        linearLayout_facebook=rootview.findViewById(R.id.linearLayout_facebook);
        name = rootview.findViewById(R.id.tv_name);
        sexe = rootview.findViewById(R.id.et_profile_sexe);



        // Here we get the person data so we can set the profile info
        name.setText(Singleton_Person.getInstance().current_person.getNom_complet());
        email.setText(Singleton_Person.getInstance().current_person.getEmail());
        phone_number.setText(String.valueOf(Singleton_Person.getInstance().current_person.getPhone_number()));
        poids.setText(String.valueOf(Singleton_Person.getInstance().current_person.getPoids()));

        if (Singleton_Person.getInstance().current_person.getSexe() == 1) {
            sexe.setText("Homme");
        } else if (Singleton_Person.getInstance().current_person.getSexe() == 0) {
            sexe.setText("Femme");
        } else {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
        }


        //On recupere la string en base64 dans les sharedpreferences
        //On la decode pour recuperer un bitmap et on la place a la place de la profile_photo
        if (getSharedPreferences(getContext()).getString("profile_bitmap", "").length()!=0){
            String base64_bitmap = getSharedPreferences(getContext()).getString("profile_bitmap", "");
            profile_photo.setImageBitmap(base64ToBitmap(base64_bitmap));
        }




        //Floating boutton pour changer la photo de profil
        fab_for_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 3);

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                /**
                 AlertDialog builder for confirmation of log out
                 */

                // Set Title and Message:
                builder.setTitle("Confirmation").setMessage("Vous voulez vous deconnecter ?");
                builder.setCancelable(true);
                builder.setIcon(R.drawable.logout);

                // Create "Positive" button with OnClickListener.
                builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        setUserName(getContext(), "");
                        setBitmap_profile(getContext(), "");
                        Intent myService = new Intent(getContext(), Alcohol_updater.class);
                        getActivity().getApplicationContext().stopService(myService);
                        Singleton_Taux_alcool.getInstance().setTaux_alcool(0);
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        startActivity(intent);
                        Toast.makeText(getContext(), "Deconnexion !", Toast.LENGTH_SHORT).show();
                    }
                });

                // Create "Negative" button with OnClickListener.
                builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Cancel
                        dialog.cancel();
                    }
                });

                // Create AlertDialog:
                AlertDialog alert = builder.create();
                alert.show();

            }
        });


        linearLayout_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 1. Appeler une URL web
                String url = "https://www.facebook.com/groups/1023717125215470";
                Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
                startActivity(intent);
            }
        });



        return rootview;
    }


        @Override
        public void onActivityResult ( int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == RESULT_OK && data != null) {
                Uri selectedImage = data.getData();
                profile_photo.setImageURI(selectedImage);
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getActivity().getContentResolver(), selectedImage);
                    setBitmap_profile(getContext(), encodeBitmapTobase64(bitmap));

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

    static final String PREF_USER_NAME= "username";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserName(Context ctx, String userName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static String getUserName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static void setBitmap_profile(Context ctx, String base64_bitmap)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString("profile_bitmap", base64_bitmap);
        editor.commit();
    }



    public static String encodeBitmapTobase64(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] byteArray = baos.toByteArray();
        String encodedImageString =
                Base64.getEncoder().encodeToString(byteArray);
        return encodedImageString ;
    }

    public static Bitmap base64ToBitmap(String encodedString) {

        byte[] decodedString = android.util.Base64.decode(encodedString, android.util.Base64.DEFAULT);
        Bitmap bitmap= BitmapFactory.decodeByteArray(decodedString , 0,
                decodedString.length);
        return bitmap;
    }



}