package com.training.watertracker;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Fragment_Measurement extends Fragment {



    public static Fragment_Measurement newInstance(String param1, String param2) {
        Fragment_Measurement fragment = new Fragment_Measurement();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment__measurement__and__tips, container, false);
        return rootView;
    }
}