package com.training.watertracker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.training.watertracker.services.Alcohol_updater;

public class MainActivity extends AppCompatActivity implements onRecyclerItemClickListener{

    private String TAG = "Debug Antoine";
    RecyclerView recyclerView;
    Person currentPerson;
    SharedPreferences sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(nabListener);


        //on create we set the fragment is homefragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new HomeFragment()).commit();

        currentPerson = Singleton_Person.getInstance().getCurrent_person();
    }




    //bottom navigation listener
    private BottomNavigationView.OnNavigationItemSelectedListener nabListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;
                    switch (item.getItemId()){
                        case R.id.mainActivity:
                            selectedFragment = new HomeFragment();
                            break;
                        case R.id.profile_Fragment:
                            selectedFragment = new Profile_Fragment();
                            break;
                        case R.id.liveFragment:
                            selectedFragment = new Fragment_Live();
                        default:


                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            selectedFragment).commit();
                    return true;
                };
            };

    @Override
    public void onClickListener(View view, int position) {

    }
}