package com.training.watertracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.PreferenceManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class LoginActivity extends AppCompatActivity implements CallBackFragment, SendPerson{


    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        addFragment();

    }

    public void addFragment(){
        Fragment_Login fragment = new Fragment_Login();
        fragment.setCallBackFragment(this);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container_login_register, fragment);
        fragmentTransaction.commit();
    }

    public void replaceFragment_for_register(){
        fragment = new Fragment_Register();
        fragmentManager =getSupportFragmentManager();
        fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.fragment_container_login_register, fragment);
        fragmentTransaction.commit();
    }



    @Override
    public void change() {
        replaceFragment_for_register();
    }

    @Override
    public void sendPerson(Person person) {
        //On envoie la personne de transition au fragment register 2 pour finir la registration
        Fragment_register_2 fragment_register_2 = new Fragment_register_2();
        fragment_register_2.updatePerson(person);

        //On remplace le fragment register1 par register2 quand on fait sendPerson
        // Donc quand on click sur suivant
        fragmentManager  = getSupportFragmentManager();
        fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.fragment_container_login_register, fragment_register_2);
        fragmentTransaction.commit();
    }



}