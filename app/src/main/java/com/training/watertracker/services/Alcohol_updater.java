package com.training.watertracker.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.util.Log;

import com.training.watertracker.MainActivity;
import com.training.watertracker.Person;
import com.training.watertracker.R;
import com.training.watertracker.Singleton_Person;
import com.training.watertracker.Singleton_Taux_alcool;

import java.text.DecimalFormat;

public class Alcohol_updater extends Service {



    Person current_person = Singleton_Person.getInstance().current_person;



    public Alcohol_updater() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){



                    try {
                        Log.d("Debug Antoine Service", "Service is running");
                        if (Singleton_Taux_alcool.getInstance().getTaux_alcool()>0.50) {
                            final String CHANNELID = "Foreground Service Id";
                            NotificationChannel channel = new NotificationChannel(
                                    CHANNELID,
                                    CHANNELID,
                                    NotificationManager.IMPORTANCE_LOW
                            );
                            Intent activityIntent = new Intent(getApplicationContext(), MainActivity.class);
                            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(),
                                    0,activityIntent, 0);
                            getSystemService(NotificationManager.class).createNotificationChannel(channel);
                            DecimalFormat df = new DecimalFormat("#.##");
                            String strDouble = df.format(Singleton_Taux_alcool.getInstance().getTaux_alcool());
                            Notification.Builder notification = new Notification.Builder(getApplicationContext(), CHANNELID)
                                    .setContentText("Taux d'alcool actuel : " + strDouble)
                                    .setContentTitle("Ne prenez pas la route !")
                                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.watterbottle))
                                    .setSmallIcon(R.drawable.watterbottle)
                                    .setContentIntent(contentIntent);


                            startForeground(1001, notification.build());


                            Log.d("Debug Antoine Service", "" + Singleton_Taux_alcool.getInstance().getTaux_alcool());
                        }else if(Singleton_Taux_alcool.getInstance().getTaux_alcool()<0.50 && Singleton_Taux_alcool.getInstance().getTaux_alcool()>0){
                            final String CHANNELID = "Foreground Service Id";
                            NotificationChannel channel = new NotificationChannel(
                                    CHANNELID,
                                    CHANNELID,
                                    NotificationManager.IMPORTANCE_LOW
                            );
                            getSystemService(NotificationManager.class).createNotificationChannel(channel);
                            Intent activityIntent = new Intent(getApplicationContext(), MainActivity.class);
                            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(),
                                    0,activityIntent, 0);
                            DecimalFormat df = new DecimalFormat("#.##");
                            String strDouble = df.format(Singleton_Taux_alcool.getInstance().getTaux_alcool());
                            Notification.Builder notification = new Notification.Builder(getApplicationContext(), CHANNELID)
                                    .setContentText("Taux d'alcool actuel : " + strDouble)
                                    .setContentTitle("Vous pouvez reprendre la route !")
                                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.watterbottle))
                                    .setSmallIcon(R.drawable.watterbottle)
                                    .setContentIntent(contentIntent);


                            startForeground(1001, notification.build());
                        }
                        else{
                            NotificationManager notifManager= (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                            notifManager.cancelAll();
                            stopForeground(true);
                            stopSelf();
                            onDestroy();
                        }
                        Thread.sleep(900000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    if (current_person.getSexe() == 1) {

                        double new_taux = Singleton_Taux_alcool.instance.getTaux_alcool()-0.025;
                        if (new_taux<0){new_taux=0;}
                        Singleton_Taux_alcool.getInstance().setTaux_alcool(new_taux);
                    } else if (current_person.getSexe() == 0) {

                        double new_taux = Singleton_Taux_alcool.instance.getTaux_alcool()-0.021;
                        if (new_taux<0){new_taux=0;}
                        Singleton_Taux_alcool.getInstance().setTaux_alcool(new_taux);

                    }


                }
            }
        }).start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.i("Debug Antoine", "onCreate() , service stopped...");
    }
}