package com.training.watertracker;

import java.io.Serializable;

public class Boisson implements Serializable {


    private String nomBoisson;
    private int pourcentage_alcool;
    private int drawable_photo;
    private int cL;

    public Boisson(String nomBoisson, int pourcentage_alcool, int drawable_photo, int cL) {
        this.nomBoisson = nomBoisson;
        this.cL=cL;
        this.pourcentage_alcool = pourcentage_alcool;
        this.drawable_photo =drawable_photo;
    }



    public String getNomBoisson() {
        return nomBoisson;
    }

    public void setNomBoisson(String nomBoisson) {
        this.nomBoisson = nomBoisson;
    }

    public int getPourcentage_alcool() {
        return pourcentage_alcool;
    }

    public void setPourcentage_alcool(int pourcentage_alcool) {
        this.pourcentage_alcool = pourcentage_alcool;
    }
    public int getcL(){
        return cL;
    }

    public void setcL(int cL) {
        this.cL = cL;
    }

    public int getDrawable_photo() {
        return drawable_photo;
    }

    public void setDrawable_photo(int drawable_photo) {
        this.drawable_photo = drawable_photo;
    }



}
