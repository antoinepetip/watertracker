package com.training.watertracker;

import static com.training.watertracker.Fragment_Login.getUserName;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Launcher extends AppCompatActivity {

    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance("https://profilepage-afbad-default-rtdb.europe-west1.firebasedatabase.app/");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                //demarrer la page d'acceuil si l'utilisateur est deja connecté
                // ou la page de connexion
                alreadylogged();
            }
        };
        //handler post delayed
        new Handler().postDelayed(runnable, 3000);

    }
    private void alreadylogged() {

        if (getUserName(getApplicationContext()).length()!=0) {
            DatabaseReference reference = firebaseDatabase.getReference("Users");

            Query checkUser = reference.orderByChild("nom_complet").equalTo(getUserName(getApplicationContext()));

            checkUser.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {



                        String emailFromDB = snapshot.child(getUserName(getApplicationContext())).child("email").getValue(String.class);
                        int phoneFromDB = snapshot.child(getUserName(getApplicationContext())).child("phone_number").getValue(Integer.class);
                        int poidsFromDB = snapshot.child(getUserName(getApplicationContext())).child("poids").getValue(Integer.class);
                        int sexeFromDB = snapshot.child(getUserName(getApplicationContext())).child("sexe").getValue(Integer.class);

                        Person loginPerson = new Person(getUserName(getApplicationContext()), emailFromDB, poidsFromDB, phoneFromDB, sexeFromDB);
                        Singleton_Person.getInstance().setCurrent_person(loginPerson);
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                        startActivity(intent);
                    }
                    else{
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getApplicationContext(), "Il y a eu une erreur", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }


            });
        }else{
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }


    }
    static final String PREF_USER_NAME= "";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserName(Context ctx, String userName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static String getUserName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }
}