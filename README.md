# WaterTracker

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com)  [![forthebadge](http://forthebadge.com/images/badges/powered-by-electricity.svg)](http://forthebadge.com)

Description du projet :
    WaterTracker est une application qui permet de suivre la consommation d'alcool.
    Elle permet ainsi de savoir son taux d'alcoolémie en direct, mais aussi de savoir quand est-ce que l'on peut reprendre la route et les risques judiciaires encouru à un taux d'alcoolémie t.

## Pour commencer

Build du projet avec gradle requis.

### Pré-requis

Ce qu'il est requis pour commencer le projet...

- Android Studio


## Démarrage

Lancer le projet simplement en cliquant sur build et Run the app.

## Fabriqué avec


* [Android Studio](https://developer.android.com)
* [FireBase](https://firebase.google.com/) - Stockage de données




## Auteurs
Listez le(s) auteur(s) du projet ici !
* **Antoine Petipont** _alias_ [antoinepetip](https://github.com/antoinepetip)



